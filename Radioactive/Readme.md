CLI-based radio write on *Python*

1)mkdir Radioactive

2)cd Radioactive

3)python3 -m venv venv

4)venv/bin/pip install --upgrade radio-active
![Alt text](<search radio.png>)

5)venv/radio --search [STATION_NAME]

6)select ID radiostation
![Alt text](<enjoy.png>)
Enjoy