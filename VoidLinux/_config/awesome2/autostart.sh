#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run "mpd"
run "nm-applet"
run "setxkbmap -model pc105 -layout us,ru -option grp:alt_shift_toggle"
run "fbxkb"
run "pipewire"
run "udiskie"
