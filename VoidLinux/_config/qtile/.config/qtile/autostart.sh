#!/usr/bin/env bash
#

feh --bg-scale ~/Images/wall4.jpg &
pgrep -x volumeicon >/dev/null || volumeicon &
picom &
pgrep -x dunst || dunst &
pgrep -x nm-applet || nm-applet &
