#!/usr/bin/env bash
#

feh --bg-scale ~/Images/Fn.jpg &
nm-applet &
xset s off &
xset dpms 0 0 0 &
pgrep -x volumeicon >/dev/null || volumeicon &
#flameshot &
picom &
pipewire &
#pipewire-pulse &
#wireplumber &
#telegram-desktop &
