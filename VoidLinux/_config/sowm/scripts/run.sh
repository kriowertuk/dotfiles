#!/bin/sh

xrdb merge ~/.Xresources
xbacklight -set 10 &
feh --bg-fill ~/Images/Fn.jpg &
#picom &
volumeicon &
setxkbmap -model pc105 -layout us,ru -option grp:alt_shift_toggle &
pipewire &
dunst &
nm-tray &
fbxkb &

cairo-dock &
while type sowm >/dev/null; do sowm && continue || break; done
